# Maintainer: tioguda <guda.flavio@gmail.com>

pkgbase=droidcam
kernel=5.11
_extramodules=extramodules-${kernel}-MANJARO
_linuxprefix=linux${kernel/./}
pkgname=${_linuxprefix}-${pkgbase}
_pkgver=1.7.3
pkgver=1.7.3_5.11.22_2
pkgrel=1
pkgdesc='Driver for Droidcam'
arch=('x86_64')
url="https://github.com/aramg/${pkgbase}"
license=('GPL')
makedepends=("${_linuxprefix}" "${_linuxprefix}-headers")
provides=("${pkgbase}-modules=${kernel}" "${_linuxprefix}-${pkgbase}")
conflicts=("${pkgbase}-dkms" "${pkgbase}-dkms-git")
groups=("$_linuxprefix-extramodules")
install="${pkgbase}.install"
source=("${pkgbase}-${_pkgver}.zip::${url}/archive/v${_pkgver}.zip")
sha512sums=('3934033dac931277a2f8ff348bcaa39b0cfe3e73885acd28f34b4b4efd8ce0b8606f23493b92206b5a7d3a2e1a2e1726d1d9ec33cd3f1876d1e6806dfb59c74f')

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

prepare() {
    cd "${srcdir}/${pkgbase}-${_pkgver}"
}

build() {
    _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"
    cd "${srcdir}/${pkgbase}-${_pkgver}"

    make -C /usr/lib/modules/$_kernver/build \
    M="${srcdir}/${pkgbase}-${_pkgver}/v4l2loopback" \
    modules
}

package() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    depends=("${_linuxprefix}=${_ver}")

    cd "${srcdir}/${pkgbase}-${_pkgver}"

    install -Dm755 "v4l2loopback/v4l2loopback-dc.ko" "${pkgdir}/usr/lib/modules/${_extramodules}/v4l2loopback-dc.ko"
    find "${pkgdir}" -name '*.ko' -exec xz {} +

    # set the kernel we've built for inside the install script
    sed -i -e "s/EXTRAMODULES=.*/EXTRAMODULES=${_extramodules}/g" "${startdir}/${pkgbase}.install"
}

